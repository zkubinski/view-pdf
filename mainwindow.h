#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QString>
#include <QVector>
#include <QVariant>
#include "connect_db.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
   
    int licznikN=0;
    int licznikP=0, licznikP2=0;

    myConnectDB *myConnDB;

    QVariant mySizeDB;

    QVector <QString> myStrID;
    QVector <QString> myStrPath;

    QString myID;
    QString myPath;

private slots:
    bool BNext();
    bool BPrev();

signals:
    void NoNP(bool);
};
#endif // MAINWINDOW_H
