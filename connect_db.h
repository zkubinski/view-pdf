#ifndef CONNECT_DB_H
#define CONNECT_DB_H

#include <QtSql/QSqlDatabase>

class myConnectDB : public QSqlDatabase
{
public:
    myConnectDB();
    ~myConnectDB();

    bool connectStatus();

private:
    bool status=true;
    QSqlDatabase connect;
};

#endif // CONNECT_DB_H
