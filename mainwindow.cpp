#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSqlQuery>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    myConnDB = new myConnectDB();
    myConnDB->connectStatus();

    QSqlQuery myQuery(*myConnDB);
    myQuery.prepare("SELECT pID, pSciezka FROM tDane");
    myQuery.exec();
    myQuery.isSelect();

    while (myQuery.next()) {
        myID = myQuery.value(0).toString();
        myPath = myQuery.value(1).toString();

        myStrID.push_back(myID);
        myStrPath.push_back(myPath);
    }

    mySizeDB = myQuery.size();

    licznikP = mySizeDB.toInt();

    licznikP2=licznikP-2;

    QObject::connect(ui->pushBurrNext, &QPushButton::clicked, this, &MainWindow::BNext);
    QObject::connect(this, &MainWindow::NoNP, ui->pushBurrNext, &QPushButton::setDisabled);

    QObject::connect(ui->pushButtPrev, &QPushButton::clicked, this, &MainWindow::BPrev);
//    QObject::connect(this, &MainWindow::NoNP, ui->pushButtPrev, &QPushButton::setDisabled);

    ui->Labelilosc->setText(mySizeDB.toString());
}

bool MainWindow::BNext()
{
    if(licznikN == (mySizeDB.toInt()-1)){
        emit NoNP(true);
    }

    ui->myuiID->setText(myStrID.at(licznikN));
    ui->myuiPath->setText(myStrPath.at(licznikN));

    licznikN++;
    qDebug()<<"click next"<<licznikN;
    return true;
}

bool MainWindow::BPrev()
{
    if(licznikP2 > 0) {
        emit NoNP(false);
    }
    if(licznikP2==0){
        emit NoNP(true);
        ui->pushBurrNext->setEnabled(true);
        ui->pushButtPrev->setEnabled(false);
    }

    ui->myuiID->setText(myStrID.at(licznikP2));
    ui->myuiPath->setText(myStrPath.at(licznikP2));

    qDebug()<<"click prev"<<licznikP2;

    licznikP2--;

    return true;
}

MainWindow::~MainWindow()
{
    delete myConnDB;
    delete ui;
}
