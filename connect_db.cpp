#include "connect_db.h"

#include <QDebug>

myConnectDB::myConnectDB()
{
//    connect = QSqlDatabase::addDatabase("QSQLITE");
//    connect.setDatabaseName(QString("boi3.db"));

    connect = QSqlDatabase::addDatabase("QMARIADB");
    connect.setHostName(QString("127.0.0.1"));
    connect.setDatabaseName(QString("DBViewPDF"));
    connect.setPort(3306);
    connect.setUserName(QString("root"));
    connect.setPassword(QString(""));
}

bool myConnectDB::connectStatus()
{
    if(connect.open()==status){
        qDebug()<<"połączono";
        return true;
    }
    else{
        qDebug()<<"brak połączenia";
        return false;
    }
}

myConnectDB::~myConnectDB()
{
    connect.close();
}
